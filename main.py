import openpyxl
import requests
import logging
import asyncio
import json
import sys
import os
from aiogram import Bot, executor, Dispatcher
from aiogram.types import (Message,
                           InputFile)
from pyppeteer import launch
from bs4 import BeautifulSoup
from bs4.element import Tag, NavigableString


with open('template_.json', 'r') as js_fl:
    dc = json.load(js_fl)
TOKEN = dc['token']


bot = Bot(TOKEN, parse_mode='html')
dp = Dispatcher(bot)

logger = logging.getLogger(__name__)

logging.basicConfig(
    level=logging.WARNING,
    format='[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s',
    handlers=[
        logging.StreamHandler(sys.stdout)
    ]
)


@dp.message_handler(commands=['start'])
async def start_bot(message: Message):
    chat_id = message.chat.id
    text = "Отправьте соответствующий URL-адрес на сайт Avito."
    await bot.send_message(chat_id, text)


@dp.message_handler(lambda m:True)
async def get_url(message: Message):
    chat_id = message.chat.id
    url = message.text
    text = "Данные обрабатываются"
    mess = await bot.send_message(chat_id, text)
    brow = await launch(headless=True, options={'args': ['--no-sandbox']})
    page = await brow.newPage()
    offset = 0
    limit = 100
    try:
        key = message.text.replace('/', ' ').split()[3]
        url = "https://www.avito.ru/web/5/user/" + key
        url += f"/ratings?limit={limit}&offset={offset}&sortRating=date_desc&summary_redesign=1"
        temp = "https://www.avito.ru"
        resp = []
        for i in range(10):
            await page.goto(url)
            cont = await page.content()
            name = f'offser_{offset}'
            arr, url = parser(cont, name)
            if url is None:
                resp.extend(arr)
                break
            url = temp + url
            resp.extend(arr)

        await brow.close()
    except Exception as err:
        await brow.close()
        logger.error(err)
        await message.reply('Не удалось получить данные с этого URL')
        return
    try:
        workbook = openpyxl.Workbook()
        sheet = workbook.active
        row = 1
        date = "Дата"
        serv = "Товар/Услуга"
        # link = "Ссылка на профиль"
        sheet.cell(row, 1, date)
        sheet.cell(row, 2, serv)
        # sheet.cell(3, col, link)
        logger.error(len(resp))
        for i in resp:
            row += 1
            sheet.cell(row, 1, i[0])
            sheet.cell(row, 2, i[1])
        if not os.path.exists('files'):
            os.mkdir('files')
        workbook.save('files/reviews.xlsx')
        
        file = InputFile('files/reviews.xlsx')
        await bot.delete_message(chat_id, mess.message_id)
        await bot.send_document(chat_id, file)
        logger.info('sending')
    except Exception as err:
        logger.error(err)
        await brow.close()


def parser(cont, name):
    soup = BeautifulSoup(cont, 'lxml')
    js = soup.find('pre').text
    js = json.loads(js)
    with open(f'files/{name}.json', 'w', encoding='utf-8-sig') as file:
        file.write(json.dumps(js, indent=2, ensure_ascii=False))
    
    arr = []
    try:
        for i in js['entries']:
            try:
                if 'rated' in i['value'].keys() and 'itemTitle' in i['value'].keys():
                    rated = i['value']['rated']
                    title = i['value']['itemTitle']
                else:
                    continue
            except Exception as err:
                logger.error(err)
                continue
            arr.append([rated, title])
        next_page = js['nextPage'] if 'nextPage' in js.keys() else None
        return arr, next_page
    except Exception as err:
        logger.error(err)
        
if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)